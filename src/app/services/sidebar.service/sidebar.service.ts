import { Injectable } from '@angular/core';

import { LOCAL_STORAGE_SIDEBAR } from '../../shared/constants/constans';
import { LocalStorageService } from '../local-storage.service/local-storage.service';

@Injectable()
export class SidebarService {
  public isSidebarVisible: boolean = false;
  public isVisibleText: boolean = this.getStateBreakPointText();
  public isVisibleIcons: boolean = SidebarService.getStateBreakPointIcons();

  constructor(
    public localStorageService: LocalStorageService,
  ) {}

  public toggleSidebarVisibility(): void {
    if (!this.isVisibleIcons) {
      this.isVisibleText = !this.isVisibleText;

      this.localStorageService.setItemSidebar({
        key: LOCAL_STORAGE_SIDEBAR,
        value: JSON.stringify(this.isVisibleText),
      });
    }

    if (this.isVisibleIcons) {
      this.isVisibleText = true;
      this.isSidebarVisible = !this.isSidebarVisible;
    }
  }

  public onResize(): void {
    this.isSidebarVisible = false;
    this.isVisibleText = this.getStateBreakPointText();
    this.isVisibleIcons = SidebarService.getStateBreakPointIcons();
  }

  private getStateBreakPointText(): boolean {
    if (this.localStorageService.getItemSidebar(LOCAL_STORAGE_SIDEBAR) === null) return window.innerWidth >= 1000;

    return this.localStorageService.getItemSidebar(LOCAL_STORAGE_SIDEBAR);
  }

  private static getStateBreakPointIcons(): boolean {
    return window.innerWidth <= 600;
  }
}