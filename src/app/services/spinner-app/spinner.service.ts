import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
  public isLoading: boolean = false;

  public showSpinner(): void {
    this.isLoading = true;
  }

  public hideSpinner(): void {
    this.isLoading = false;
  }
}
