import { Injectable } from '@angular/core';
import { catchError, map, Observable, retry } from 'rxjs';

import { ErrorService } from '../error/error.service';
import { UrlService } from '../url.service/url.service';
import { getUniqId } from '../../shared/helepers/uniqs';
import { PopupService } from '../popup.service/popup.service';
import { PRODUCT_NAME } from '../../shared/constants/product-name';
import { RequestService } from '../request.service/request.service';
import { Product, ProductData, ProductDataApi } from '../../shared/models/productApi';

@Injectable()
export class ProductService {
  public isSort: boolean = true;

  private count: number = 0;

  private readonly PRODUCT_NAME: string[] = PRODUCT_NAME;

  constructor(
    public urlService: UrlService,
    public popupService: PopupService,
    public requestService: RequestService,
    public errorService: ErrorService,
  ) { }

  public getProductsData$(category: string): Observable<ProductData> {
    return this.requestService.getProduct(this.urlService.getProductUrl(category))
      .pipe(
        retry(3),
        catchError(this.errorService.handleError),
        map((data: ProductDataApi): ProductData => {
          return ProductData.getProductDataFromApi(data);

        }))
  }

  // попытка удаления с сервера
  // public deleteProductByAuth$(str: string): Observable<ProductData> {
  //   return this.requestService.delete(`https://api.publicapis.org/entries?/${str}`)
  //     .pipe(
  //       catchError(this.errorService.handleError),
  //       map((data: ProductDataApi): ProductData => ProductData.getProductDataFromApi(data)));
  // }

  public sort(data: Product[], str: string): Product[] {
    data.sort((elem: any, item: any) => {
      if (this.isSort) {
        if (elem[str].toLowerCase() > item[str].toLowerCase()) {
          return -1;
        } else if (elem[str].toLowerCase() < item[str].toLowerCase()) {
          return 1;
        }
        return 1;
      } else {
        return -1;
      }
    });
    this.isSort = !this.isSort;

    return data;
  }

  public removeProduct(products: Product[], data: Product): Product[] {
    let index = products.findIndex(item => {
      return item.id === data.id;
    });

    if (index === -1) {
      this.popupService.openPopupConfig(this.popupService.CONFIG_ERROR);
    }
    products.splice(index, 1);

    return products;
  }

  public addProduct(data: Product[], elem: Product): Product[] {
    data.unshift({
      id: getUniqId(),
      male: elem.male,
      api: elem.api,
      auth: this.getProductName(),
      category: elem.category,
      cors: this.getProductName(),
      description: elem.description,
      https: true,
      link: this.getProductName(),
    });
    console.log('adding', elem);

    return data;
  }

  private getProductName(): string {
    let result: string = '';

    if (this.count < this.PRODUCT_NAME.length) {
      result = this.PRODUCT_NAME[this.count];
      this.count++;
    }

    if (this.count === this.PRODUCT_NAME.length) {
      this.count = 0;
    }

    return result;
  }
}
