import { ErrorService } from './error/error.service';
import { UrlService } from './url.service/url.service';
import { PopupService } from './popup.service/popup.service';
import { SpinnerService } from './spinner-app/spinner.service';
import { RequestService } from './request.service/request.service';
import { SidebarService } from './sidebar.service/sidebar.service';
import { ProductService } from './product.service/product.service';
import { LocalStorageService } from './local-storage.service/local-storage.service';

export const SERVICES = [
  UrlService,
  PopupService,
  ErrorService,
  SpinnerService,
  RequestService,
  ProductService,
  SidebarService,
  LocalStorageService,
]