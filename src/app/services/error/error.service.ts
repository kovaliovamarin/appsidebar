import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ErrorService {
  public handleError(error: HttpErrorResponse): any {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(`Error ${error.status}, body was: `, error.error);
    }

    return throwError(() => new Error('Something bad happened; please try again later'));
  }
}