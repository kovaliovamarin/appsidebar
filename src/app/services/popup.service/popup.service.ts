import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

import { Product } from '../../shared/models/productApi';
import { ResponsData } from '../../shared/models/respons-data';
import { ModalProduct } from '../../shared/models/modal-product';
import { CONFIG_ERROR } from '../../shared/constants/popup/config-error';
import { PopupNotification } from '../../shared/models/popup-notification';
import { CONFIG_PRODUCT_ADD } from '../../shared/constants/config-product-add';
import { CONFIG_PRODUCT_EDIT } from '../../shared/constants/config-product-edit';
import { REMOVE_PRODUCT_ITEM } from '../../shared/constants/popup/remove-product-modal';
import { INFORMATION_FOR_REMOVE } from '../../shared/constants/popup/information-for-remove';
import { CONFIG_FOR_TABLE_NATYFI } from '../../shared/constants/popup/config-for-table-natifi';
import { CONFIG_FOR_HEADER_NATYFI } from '../../shared/constants/popup/config-for-header-natifi';

@Injectable()
export class PopupService {
  public isOpenModalConfig: boolean = false;
  public modalProduct: ModalProduct = new ModalProduct();
  public confirmData: PopupNotification = new PopupNotification();
  public isClosePopup: Subject<ResponsData> = new Subject<ResponsData>();

  public readonly CONFIG_ERROR: PopupNotification = CONFIG_ERROR;
  public readonly CONFIG_PRODUCT_ADD: ModalProduct = CONFIG_PRODUCT_ADD;
  public readonly CONFIG_PRODUCT_EDIT: ModalProduct = CONFIG_PRODUCT_EDIT;
  public readonly CONFIG_DATA: PopupNotification = CONFIG_FOR_TABLE_NATYFI;
  public readonly CONFIG_DATA_REMOVE: PopupNotification = INFORMATION_FOR_REMOVE;
  public readonly CONFIG_DATA_HEADER: PopupNotification = CONFIG_FOR_HEADER_NATYFI;

  public openPopupConfig(config: PopupNotification): void {
    this.confirmData = config;
    this.isOpenModalConfig = true;
  }

  public openModalConfig(config: ModalProduct): void {
    this.modalProduct = config;
  }

  public closePopup(data: ResponsData): void {
    this.confirmData = new PopupNotification();
    this.isOpenModalConfig = false;
    this.isClosePopup.next(data);
  }

  public openModalRemove(data: Product): void {
    this.openPopupConfig({
      title: REMOVE_PRODUCT_ITEM.title,
      bodyText: REMOVE_PRODUCT_ITEM.bodyText + data.api + '?',
      isOkBtn: REMOVE_PRODUCT_ITEM.isOkBtn,
      isDelBtn: REMOVE_PRODUCT_ITEM.isDelBtn,
      data: data,
    });
  }
}
