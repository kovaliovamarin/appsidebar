import { Injectable } from '@angular/core';

import { LocalStorageData } from '../../shared/models/local-storage-data';

@Injectable()
export class LocalStorageService {

  public getLocalStorage(): Storage {
    return localStorage;
  }

  public setItemSidebar(data: LocalStorageData): void {
    this.getLocalStorage().setItem(data.key, data.value);
  }

  public getItemSidebar(key: string): boolean {
    return JSON.parse(String(this.getLocalStorage().getItem(key) ));
  }
}