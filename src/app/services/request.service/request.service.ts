import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RequestService {
  constructor(
    private http: HttpClient,
  ) {}

  public getProduct(url: string): any {
    return this.http.get(url);
  }

  public post(url: string, body: any): any {
    return this.http.post(url, body);
  }

  public delete(url: string): any {
    return this.http.delete(url);
  }
}