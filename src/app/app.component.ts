import { Component, HostListener } from '@angular/core';

import { PopupService } from './services/popup.service/popup.service';
import { SidebarService } from './services/sidebar.service/sidebar.service';
import { ProductService } from './services/product.service/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public tipsButtonName: string = 'TIPS';

  public readonly BACKGROUND: string = '';

  constructor(
    public popupService: PopupService,
    public sidebarService: SidebarService,
    public productService: ProductService,
  ) {}

  @HostListener('click', ['$event.target'])
  public onClick(elem: Element): void {
    if (this.sidebarService.isSidebarVisible) {
      if (elem.closest('.sidebar__menu-item') || elem.closest('.container__main-mobile')) {
        this.sidebarService.toggleSidebarVisibility();
      }
    }
  }
}


