import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { APP_PAGES } from './pages';
import { SERVICES } from './services';
import { NgxMaskModule } from 'ngx-mask';
import { APP_COMPONENT } from './component';
import { AppComponent } from './app.component';
import { SHARED_COMPONENT } from './shared/components';
import { MatListModule } from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    APP_PAGES,
    AppComponent,
    APP_COMPONENT,
    SHARED_COMPONENT,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    MatListModule,
    MatSortModule,
    MatSidenavModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [
    SERVICES,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
