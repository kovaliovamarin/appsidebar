import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { User } from './pages/user/user';
import { Tips } from './pages/tips/tips';
import { Aheets } from './pages/aheets/aheets';
import { FormPage } from './pages/form-page/form-page';
import { Accounts } from './pages/accounts/accounts';
import { Products } from './pages/products/products';
import { Requests } from './pages/requests/requests';
import { AppTable } from './pages/app-table/app-table';
import { Suppliers } from './pages/suppliers/suppliers';
import { MyProfile } from './pages/my-profile/my-profile';
import { PriceSheet } from './pages/price-sheet/price-sheet';
import { NewRequest } from './pages/new-request/new-request';
import { InvoiceSummary } from './pages/invoice-summary/invoice-summary';
import { UserManagement } from './pages/user-management/user-management';

const routes: Routes = [
  {
    path: 'user',
    component: User,
  },
  {
    path: '',
    redirectTo: '/new-request',
    pathMatch: 'full',
  },
  {
    path: 'form-page',
    component: FormPage,
  },
  {
    path: 'accounts',
    component: Accounts,
  },
  {
    path: 'products',
    component: Products,
  },
  {
    path: 'requests',
    component: Requests,
  },
  {
    path: 'suppliers',
    component: Suppliers,
  },
  {
    path: 'my-profile',
    component: MyProfile,
  },
  {
    path: 'price-sheet',
    component: PriceSheet,
  },
  {
    path: 'new-request',
    component: NewRequest,
  },
  {
    path: 'aheets',
    component: Aheets,
  },
  {
    path: 'invoice-summary',
    component: InvoiceSummary,
  },
  {
    path: 'user-management',
    component: UserManagement,
  },
  {
    path: 'table',
    component: AppTable,
  },
  {
    path: 'hover-tips',
    component: Tips,
  },
  {
    path: '**',
    redirectTo: '/new-request',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
