import { Component, Input } from '@angular/core';

@Component({
  selector: 'hover-tips',
  templateUrl: './hover-tips.component.html',
  styleUrls: ['./hover-tips.component.scss'],
})
export class HoverTipsComponent {
  @Input() hoverTips: string[] = [];

  public count: number = 0;

  public visibilityText(event: MouseEvent) {
    if ( this.count > 0 ) {
      this.count--;
    } else {
      this.count = this.hoverTips.length - 1;
    }

    event.preventDefault();
  }

  public nextText(event: MouseEvent) {
    if ( this.count < this.hoverTips.length - 1 ) {
      this.count++;
    } else {
      this.count = 0;
    }

    event.preventDefault();
  }
}