import { Component } from '@angular/core';

import { RegistrationData } from '../../shared/models/registration-data';

@Component({
  selector: 'form-app',
  templateUrl: './form-app.component.html',
  styleUrls: ['./form-app.component.scss']
})
export class FormAppComponent {
  public isError: boolean = false;
  public registrationData: RegistrationData = new RegistrationData();

  ngOnInit(): void {
    console.log(this.registrationData);
  }

  public getData(data: RegistrationData): void {
    console.log(data);
  }
}