import { FormAppComponent } from './form-app/form-app.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { HoverTipsComponent } from './hover-tips/hover-tips.conponent';
import { ProductTableComponent } from './product-table/product-table.component';
import { ProductModalComponent } from './product-modal/product-modal.component';

export const APP_COMPONENT = [
  FormAppComponent,
  DropdownComponent,
  HoverTipsComponent,
  ProductTableComponent,
  ProductModalComponent,
]