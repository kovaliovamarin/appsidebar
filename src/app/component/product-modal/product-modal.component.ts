import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';

import { Product } from '../../shared/models/productApi';
import { PopupService } from '../../services/popup.service/popup.service';
import { CONFIG_RADIO_BTM } from '../../shared/constants/config_radio_btm';
import { ProductService } from '../../services/product.service/product.service';

@Component({
  selector: 'product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss']
})
export class ProductModalComponent {
  @Input() public product: Product;
  @Input() public dropdownMenu: string[] = [];
  @Input() public isOpenPopupAdd: boolean = false;
  @Input() public isOpenPopupEdit: boolean = false;

  @Output() public closeModalEdit: EventEmitter<void> = new EventEmitter<void>();
  @Output() public saveModalEdit: EventEmitter<Product> = new EventEmitter<Product>();

  public selectedByCategory: string = '';

  public readonly nameDropDown: string = 'Category:';
  public readonly RADIO_BTM: string[] = CONFIG_RADIO_BTM;
  public readonly NAME_BY_CATEGORY: string = "All category";

  constructor(
    public popupService: PopupService,
    public productService: ProductService,
  ) {}

  public getNameSelectedCategory(category: string): void {
    this.selectedByCategory = category;
  }

  @HostListener('click', ['$event.target'])
  public onClick(elem: Element): void {
    if (!(elem.closest('.product-modal__form')) ||
      elem.closest('.product-modal__form-button')) {
      this.closeModalEdit.emit();
    }
  }
}
