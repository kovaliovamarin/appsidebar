import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

import { Product } from '../../shared/models/productApi';
import { PopupService } from '../../services/popup.service/popup.service';
import { ProductService } from '../../services/product.service/product.service';
import { ProductTableOptions } from '../../shared/models/product-table-options';
import { PRODUCT_TABLE_OPTIONS } from '../../shared/constants/product-table-options';

@Component({
  selector: 'product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnChanges {
  @Input() public product: Product[] = [];
  @Input() public productSort: Product[] = [];

  @Output() sortProduct: EventEmitter<string> = new EventEmitter<string>();
  @Output() removeProduct: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() openModalEdit: EventEmitter<Product> = new EventEmitter<Product>();

  public visibleProduct: Product[] = [];
  public isSortForColumnApi: boolean = true;
  public isSortForColumnDescription: boolean = true;

  public productTableOptions: ProductTableOptions = PRODUCT_TABLE_OPTIONS;

  public readonly SORT_FOR_API: string = "api";
  public readonly SORT_FOR_DESCRIPTION: string = "description";

  constructor(
    public popupService: PopupService,
    public productService: ProductService,
  ) {}

  public ngOnChanges(): void {
    this.visibleProduct = !this.productSort.length ? this.product : this.productSort;
    console.log(1);
  }
}