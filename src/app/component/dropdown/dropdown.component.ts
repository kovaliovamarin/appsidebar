import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {
  @Input() public nameDropDown: string = '';
  @Input() public dropdownMenu: string[] = [];
  @Input() public isOpenPopupAdd: boolean = false;
  @Input() public isOpenPopupEdit: boolean = false;
  @Input() public productSelectByCategory: string = '';

  @Output() public choice: EventEmitter<string> = new EventEmitter<string>();

  public dropdown: any = null;
}