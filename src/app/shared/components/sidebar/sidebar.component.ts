import { Component } from '@angular/core';

import { NavItem } from '../../models/nav-item';
import { NAV_ITEMS } from '../../constants/nav-items';
import { SidebarService } from '../../../services/sidebar.service/sidebar.service';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  public readonly SIDEBAR_ITEMS: NavItem[] = NAV_ITEMS;

  constructor(
    public sidebarService: SidebarService,
  ) {}
}