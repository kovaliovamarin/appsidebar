import { Component, EventEmitter, Input, Output } from '@angular/core';

import { HOVER_TIPS } from '../../constants/hover-tips';
import { PopupService } from '../../../services/popup.service/popup.service';
import { SidebarService } from '../../../services/sidebar.service/sidebar.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
})

export class AppHeaderComponent {
  @Input() public tipsButtonName: string = '';

  @Output() public openPopup: EventEmitter<void> = new EventEmitter<void>();
  @Output() public sidebarOpen: EventEmitter<void> = new EventEmitter<void>();

  public readonly HOVER_TIPS: string[] = HOVER_TIPS;

  constructor(
    public popupService: PopupService,
    public sidebarService: SidebarService,
  ) {}
}
