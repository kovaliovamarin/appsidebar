import { SidebarComponent } from './sidebar/sidebar.component';
import { AppPopupComponent } from './app-popup/app-popup.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { SpinnerAppComponent } from './spinner-app/spinner-app.component';

export const SHARED_COMPONENT = [
  SidebarComponent,
  AppPopupComponent,
  AppHeaderComponent,
  SpinnerAppComponent,
]