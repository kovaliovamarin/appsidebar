import { Component, HostListener } from '@angular/core';

import { Product } from '../../models/productApi';
import { PopupService } from '../../../services/popup.service/popup.service';

@Component({
  selector: 'app-popup',
  templateUrl: './app-popup.component.html',
  styleUrls: ['./app-popup.component.scss']
})
export class AppPopupComponent {
 public product: Product[] = [];

  constructor(
    public popupService: PopupService,
  ) {}

  @HostListener('click', ['$event.target'])
  public onClick(elem: any): void {
    if (!(elem.closest('.app-popup__container')) ||
      elem.closest('.app-popup__button-delete') ||
      elem.closest('.app-popup__button-confirm') ||
      elem.closest('.app-popup__container-button-close')) {

      this.popupService.closePopup({ isOkBtn: false, data: null });
    }

    if (elem.closest('.app-popup__button-save')) {
      this.popupService.closePopup({ isOkBtn: true, data: this.popupService.confirmData.data });
    }
  }
}
