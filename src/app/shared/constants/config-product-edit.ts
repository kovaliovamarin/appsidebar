import { ModalProduct } from '../models/modal-product';
import { CONFIG_RADIO_BTM } from './config_radio_btm';

export const CONFIG_PRODUCT_EDIT: ModalProduct = {
  title: 'Change product',
  labelApi: 'Api:',
  labelCategory: 'Category:',
  labelDescription: 'Description:',
  isOkBtn: true,
  isDelBtn: true,
  isRadioBtn: CONFIG_RADIO_BTM,
  data: null,
}