import { PopupNotification } from '../../models/popup-notification';

export const REMOVE_PRODUCT_ITEM: PopupNotification = {
  title: 'DELETE',
  bodyText: 'Do you really want to delete an' + ' ',
  isOkBtn: true,
  isDelBtn: true,
  data: null,
}