import { PopupNotification } from '../../models/popup-notification';

export const CONFIG_ERROR: PopupNotification = {
  title: 'Error',
  bodyText: 'Sorry, an error has occurred. Try a little later',
  isOkBtn: true,
  isDelBtn: false,
  data: null,
}