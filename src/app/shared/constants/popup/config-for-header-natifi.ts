import { PopupNotification } from '../../models/popup-notification';

export const CONFIG_FOR_HEADER_NATYFI: PopupNotification = {
  title: 'Notification',
  bodyText: 'Are you sure you want to remove this lead message',
  isOkBtn: true,
  isDelBtn: false,
  data: null,
}