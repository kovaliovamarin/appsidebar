import { PopupNotification } from '../../models/popup-notification';

export const CONFIG_FOR_TABLE_NATYFI: PopupNotification = {
  title: 'Hello word',
  bodyText: 'Have a good day',
  isOkBtn: true,
  isDelBtn: false,
  data: null,
}