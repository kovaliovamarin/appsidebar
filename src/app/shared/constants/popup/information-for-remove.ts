import { PopupNotification } from '../../models/popup-notification';

export const INFORMATION_FOR_REMOVE: PopupNotification = {
  title: 'DELETE INFORMATION',
  bodyText: 'The item was successfully deleted',
  isOkBtn: false,
  isDelBtn: false,
  data: null,
}