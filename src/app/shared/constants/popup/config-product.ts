import { PopupNotification } from '../../models/popup-notification';

export const CONFIG_FOR_PRODUCT: PopupNotification = {
  title: 'INFORMATION',
  bodyText: '',
  isOkBtn: false,
  isDelBtn: false,
  data: null,
}