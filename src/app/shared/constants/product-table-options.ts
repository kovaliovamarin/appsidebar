import { ProductTableOptions } from '../models/product-table-options';

export const PRODUCT_TABLE_OPTIONS: ProductTableOptions = {
  columnFirst: 'Category',
  columnSecond: 'Api',
  columnThird: 'Description',
  columnFourth: 'Status',
  columnFifth: 'Delete',
}