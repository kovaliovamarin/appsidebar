import { NavItem } from '../models/nav-item';

export const NAV_ITEMS: NavItem[] =  [
  {
    name: 'New Request',
    link: '/new-request',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Form',
    link: '/form-page',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'User Management',
    link: '/user-management',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'User',
    link: '/user',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'My Profile',
    link: '/my-profile',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Accounts',
    link: '/accounts',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'Suppliers',
    link: '/suppliers',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Products',
    link: '/products',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'Requests',
    link: '/requests',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Invoice Summary',
    link: '/invoice-summary',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'Aheets',
    link: '/aheets',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Price Sheet Search',
    link: '/price-sheet',
    icon: './assets/icons/person.svg',
  },
  {
    name: 'Table',
    link: '/table',
    icon: './assets/icons/circle.svg',
  },
  {
    name: 'Tips',
    link: '/hover-tips',
    icon: './assets/icons/person.svg',
  },
];