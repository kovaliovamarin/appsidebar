export function getRandomPrice(): number {
  return (Math.floor(Math.random() * 100) + 1);
}

export function getUniqId(): number {
  // @ts-ignore
  return (new Date().getTime() + [1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c: any) => c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
}

// export function getNameMale(data: ProductApi): string {
//   let str: string = '';
//
//   const MAN: string = 'Man';
//   const WOMAN: string = 'Woman';
//
//   let isBoolean: boolean = data.Cors === 'no';
//
//   if (isBoolean) {
//     str = MAN;
//   } else {
//     str = WOMAN;
//   }
//
//   return str;
// }
