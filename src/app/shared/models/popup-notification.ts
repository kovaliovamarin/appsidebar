export class PopupNotification {
  title: string;
  bodyText: string;
  isOkBtn: boolean;
  isDelBtn: boolean;
  data: any;
}