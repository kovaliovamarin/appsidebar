export class ModalProduct {
  title: string;
  labelApi: string;
  labelCategory: string;
  labelDescription: string;
  isOkBtn: boolean;
  isDelBtn: boolean;
  isRadioBtn: string[];
  data: any;
}
