import { map, Observable } from 'rxjs';
import { RequestService } from '../../services/request.service/request.service';

export class ElementsDataAPI {
  data: ElementsAPI[];
  isOk: boolean;
}

export class ElementsData {
  data: Elemetns[];
  isOkBtn: boolean;

  public static getElementsDataFromApi(data: ElementsDataAPI): ElementsData {
    return {
      data: data.data.map(res => {
        return Elemetns.getElemetnsFromAPI(res);
      }),

      isOkBtn: data.isOk,
    }
  }
}

export class ElementsAPI {
  name: string;
  id: number;
  createdById: number;
  createdDateTime: number;
  updatedMemo: string;
  updatedDateTime: number;
}

export class Elemetns {
  name: string;
  id: number;
  createdById: number;
  createdDateTime: number;
  updatedMemo: string;
  updatedDateTime: number;

  public static getElemetnsFromAPI(data: ElementsAPI): Elemetns {
    return {
      name: data.name,
      id: data.id,
      createdById: data.createdById,
      createdDateTime: data.createdDateTime,
      updatedMemo: data.updatedMemo,
      updatedDateTime: data.updatedDateTime,
    }
  }
}

export class page {
  constructor(
    private requestService: RequestService,
  ) { }

  public getElements$(): Observable<ElementsData> {
    return this.requestService.getProduct('')
      .pipe(
        map((data: ElementsDataAPI): ElementsData => {
          return ElementsData.getElementsDataFromApi(data);
        })
      );
  }

}

