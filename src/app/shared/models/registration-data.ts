export class RegistrationData {
  firstName: string;
  lastName: string;
  email: string;
  mobile: number;
  password: string;
}
