import { getUniqId } from '../helepers/uniqs';

export class ProductDataApi {
  count: number;
  entries: ProductAPI[];
}

export class ProductData {
  count: number;
  data: Product[];

  public static getProductDataFromApi(data: ProductDataApi): ProductData {
    return {
      count: data.count,
       data: data.entries
        .splice(0,1321)
        .map((res, i) => {
          return Product.getProductFromAPI(res, data.entries.length, i);
        }).splice(0, 100),
    }
  }
}

export class ProductAPI {
  API: string;
  Auth: string;
  Category: string;
  Cors: string;
  Description: string;
  HTTPS: boolean;
  Link: string;
}

export class Product {
  api: string;
  auth: string;
  category: string;
  cors: string;
  description: string;
  https: boolean;
  link: string;

  //FE only
  id: number;
  male: string;

  public static getProductFromAPI(data: ProductAPI, length: number, i: number): any {
    return {
      api: data.API,
      auth: data.Auth,
      category: data.Category,
      cors: data.Cors,
      description: data.Description,
      https: data.HTTPS,
      link: data.Link,

      //FE only
      id: getUniqId(),
      male: Product.getNameMale(length, i),
    }
  }

  private static getNameMale(length: number, i: number): string {
    // debugger
    if (i < length/2) {
      return "Man"
    }
    return "Woman";
  }
}
