export class NavItem {
  name: string;
  link: string;
  icon: string;
}
