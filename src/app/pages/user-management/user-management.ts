import { Subscription } from 'rxjs';
import { Component, EventEmitter, Output } from '@angular/core';

import { Product } from '../../shared/models/productApi';
import { ResponsData } from '../../shared/models/respons-data';
import { PopupService } from '../../services/popup.service/popup.service';
import { SpinnerService } from '../../services/spinner-app/spinner.service';
import { ProductService } from '../../services/product.service/product.service';
import { CONFIG_PRODUCT_EDIT } from '../../shared/constants/config-product-edit';

@Component({
  selector: 'user-management',
  templateUrl: './user-management.html',
  styleUrls: ['./user-management.scss']
})
export class UserManagement {
  @Output() removeProduct: EventEmitter<Product> = new EventEmitter<Product>();

  public isSort: boolean = true;
  public productSelect: Product;
  public products: Product[] = [];
  public productSort: Product[] = [];
  public dropdownMenu: string[] = [];
  public isOpenPopupAdd: boolean = false;
  public isOpenPopupEdit: boolean = false;

  public readonly NAME_MAN: string = "Man";
  public readonly NAME_WOMAN: string = "Woman";
  public readonly nameDropDown: string = 'Category:';
  public readonly NAME_BY_CATEGORY: string = "All category";

  private subscriptionRemove: Subscription;
  private sortForMale: string = this.NAME_WOMAN;

  constructor(
    public popupService: PopupService,
    public productService: ProductService,
    public spinnerService: SpinnerService,
  ) {}

  public ngOnInit(): void {
    this.getAllProduct();
    this.onSubscriptionRemove();
  }

  public ngOnDestroy(): void {
    this.subscriptionRemove.unsubscribe();
  }

  public choiceDropDown(str: string): void {
    this.productSort = [];
    this.productService
      .getProductsData$(str)
      .subscribe(res => this.products = res?.data);
  }

  public closeModal(): void {
    if (this.isOpenPopupAdd) {
      this.isOpenPopupAdd = false;
    } else if (this.isOpenPopupEdit) {
      this.isOpenPopupEdit = false;
    }
  }

  public removeProducts(data: Product): void {
    this.popupService.openModalRemove(data);
  }

  public sortProduct(str: string): void {
    this.productService.sort(this.products, str);
  }

  public sortProductForMale(): void {
    this.productSort = this.products.filter((item) => {
      return item.male === this.sortForMale;
    });

    if (this.sortForMale === this.NAME_MAN) {
      this.sortForMale = this.NAME_WOMAN;
    } else {
      this.sortForMale = this.NAME_MAN;
    }
  }

  public changeSelectProduct(modal: Product): void {
    if (this.isOpenPopupEdit) {
      let index = this.products.findIndex(product => product.id === this.productSelect.id);

      if (index != -1) {
        this.products[index].api = modal.api;
        this.products[index].category = modal.category;
        this.products[index].description = modal.description;
        this.products[index].male = modal.male;
      }
    } else if (this.isOpenPopupAdd) {
      this.productService.addProduct(this.products, modal);
    }
  }

  public openModalAdd(): void {
    this.popupService.openModalConfig(this.popupService.CONFIG_PRODUCT_ADD);

    this.isOpenPopupAdd = !this.isOpenPopupAdd;
  }

  public getAllProduct(): void {
    setTimeout(() => {
      this.spinnerService.isLoading = true;
    }, 0);

    this.productService
      .getProductsData$('')
      .subscribe(res => {
        this.products = res?.data;
        this.spinnerService.hideSpinner();
        this.getDropDownMenu(res?.data);
      });
  }

  private getDropDownMenu(data: Product[]): void {
    let tempArray: string[] = [];
    data.forEach(item => tempArray.push(item.category));
    this.dropdownMenu = tempArray.filter((elem, item) => tempArray.indexOf(elem) === item);
  }

  private onSubscriptionRemove(): void {
    this.subscriptionRemove = this.popupService.isClosePopup.subscribe((res: ResponsData) => {

      if (!res.isOkBtn) return;

      if (res.isOkBtn && res.data === null) return;

      this.spinnerService.showSpinner();

      setTimeout(() => {
        this.productService.removeProduct(this.products, res.data);

        this.spinnerService.hideSpinner();

        this.popupService.openPopupConfig({
          title: this.popupService.CONFIG_DATA_REMOVE.title,
          bodyText: this.popupService.CONFIG_DATA_REMOVE.bodyText,
          isOkBtn: this.popupService.CONFIG_DATA_REMOVE.isOkBtn,
          isDelBtn: this.popupService.CONFIG_DATA_REMOVE.isDelBtn,
          data: res.data,
        });
      }, 1000);
    });
  }

  public sentDataForForm(data: Product): void {
    let index = this.products.findIndex(product => product.id === data.id);

    if (index != -1) {
      this.productSelect = data;
    }
  }

  public openModalEdit(data: Product) {
    this.sentDataForForm(data);

    this.popupService.openModalConfig({
      title: CONFIG_PRODUCT_EDIT.title,
      labelApi: CONFIG_PRODUCT_EDIT.labelApi,
      labelCategory: CONFIG_PRODUCT_EDIT.labelCategory,
      labelDescription: CONFIG_PRODUCT_EDIT.labelDescription,
      isOkBtn: CONFIG_PRODUCT_EDIT.isOkBtn,
      isDelBtn: CONFIG_PRODUCT_EDIT.isDelBtn,
      isRadioBtn: CONFIG_PRODUCT_EDIT.isRadioBtn,
      data: data,
    });

    this.isOpenPopupEdit = !this.isOpenPopupEdit;
  }
}