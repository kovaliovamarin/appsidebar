import { User } from './user/user';
import { Tips } from './tips/tips';
import { Aheets } from './aheets/aheets';
import { Accounts } from './accounts/accounts';
import { Requests } from './requests/requests';
import { FormPage } from './form-page/form-page';
import { Products } from './products/products';
import { AppTable } from './app-table/app-table';
import { Suppliers } from './suppliers/suppliers';
import { MyProfile } from './my-profile/my-profile';
import { NewRequest } from './new-request/new-request';
import { PriceSheet } from './price-sheet/price-sheet';
import { InvoiceSummary } from './invoice-summary/invoice-summary';
import { UserManagement } from './user-management/user-management';

export const APP_PAGES = [
  Tips,
  User,
  Aheets,
  Products,
  Accounts,
  AppTable,
  FormPage,
  Requests,
  Suppliers,
  MyProfile,
  NewRequest,
  PriceSheet,
  InvoiceSummary,
  UserManagement,
]