import { Subscription } from 'rxjs';
import { Component, EventEmitter, Output } from '@angular/core';

import { Product } from '../../shared/models/productApi';
import { ResponsData } from '../../shared/models/respons-data';
import { PopupService } from '../../services/popup.service/popup.service';
import { SpinnerService } from '../../services/spinner-app/spinner.service';
import { CONFIG_PRODUCT_ADD } from '../../shared/constants/config-product-add';
import { ProductService } from '../../services/product.service/product.service';
import { CONFIG_PRODUCT_EDIT } from '../../shared/constants/config-product-edit';

@Component({
  selector: 'accounts',
  templateUrl: './accounts.html',
  styleUrls: ['./accounts.scss']
})
export class Accounts {
  @Output() removeProduct: EventEmitter<Product> = new EventEmitter<Product>();

  public isSort: boolean = true;
  public productSelect: Product;
  public products: Product[] = [];
  public productSort: Product[] = [];
  public dropdownMenu: string[] = [];
  public isOpenPopupAdd: boolean = false;
  public isOpenPopupEdit: boolean = false;

  public readonly NAME_MAN: string = "Man";
  public readonly NAME_WOMAN: string = "Woman";
  public readonly nameDropDown: string = 'Category:';
  public readonly NAME_BY_CATEGORY: string = "All category";
  public readonly BE: any = {
    "data": [{
      "name": "Face to Face",
      "id": 1,
      "createdById": "00000000-0000-0000-0000-000000000000",
      "createdDateTime": "2022-04-12T23:14:10.3700721+00:00",
      "updatedMemo": "Created by migration",
      "updatedDateTime": "2022-04-12T23:14:10.3700721+00:00"
    }, {
      "name": "Lab Result",
      "id": 2,
      "createdById": "00000000-0000-0000-0000-000000000000",
      "createdDateTime": "2022-04-12T23:14:10.3700721+00:00",
      "updatedMemo": "Created by migration",
      "updatedDateTime": "2022-04-12T23:14:10.3700721+00:00"
    }, {
      "name": "Sleep Study",
      "id": 6,
      "createdById": "be00fd17-47ca-4e5e-ad4d-0b1537d567e5",
      "createdDateTime": "2022-04-12T00:00:00+03:00",
      "updatedMemo": "created",
      "updatedDateTime": "2022-04-12T00:00:00+03:00"
    }], "isOk": true
  }

  private subscriptionRemove: Subscription;
  private sortForMale: string = this.NAME_WOMAN;

  constructor(
    public popupService: PopupService,
    public productService: ProductService,
    public spinnerService: SpinnerService,
  ) { }

  public ngOnInit(): void {
    this.getAllProduct();
    this.onSubscriptionRemove();

    // console.log('BE', this.BE);
    // console.log('FE', ElementsData.getElementsDataFromApi(this.BE));
  }

  public ngOnDestroy(): void {
    this.subscriptionRemove.unsubscribe();
  }

  public choiceDropDown(str: string): void {
    this.productService
      .getProductsData$(str)
      .subscribe(res => this.products = res?.data);
  }

  public closeModal(): void {
    if (this.isOpenPopupAdd) {
      this.isOpenPopupAdd = false;
    } else if (this.isOpenPopupEdit) {
      this.isOpenPopupEdit = false;
    }
  }

  public removeProducts(data: Product): void {
    this.popupService.openModalRemove(data);

    //попытка удаления с сервера
    // this.productService
    //   .deleteProductByAuth$(data.auth)
    //   .subscribe(() => this.popupService.openModalRemove(data))
  }

  public sortProduct(str: string): void {
    this.productService.sort(this.products, str);
  }

  public sortProductForMale(): void {
    this.productSort = this.products.filter((item) => {

      if (item.male === this.sortForMale) {
        return true;
      } else {
        return false;
      }
    });

    if (this.sortForMale === this.NAME_MAN) {
      this.sortForMale = this.NAME_WOMAN;
    } else {
      this.sortForMale = this.NAME_MAN;
    }

    console.log('product', this.products);
    console.log('sort', this.productSort);
  }

  public changeSelectProduct(modal: Product): void {
    if (this.isOpenPopupEdit) {
      let index = this.products.findIndex(product => product.id === this.productSelect.id);

      if (index != -1) {
        this.products[index].api = modal.api;
        this.products[index].category = modal.category;
        this.products[index].description = modal.description;
        this.products[index].male = modal.male;
      }
    } else if (this.isOpenPopupAdd) {
      this.productService.addProduct(this.products, modal);
    }
  }

  public openModalAdd(): void {
    this.popupService.openModalConfig(CONFIG_PRODUCT_ADD);

    this.isOpenPopupAdd = !this.isOpenPopupAdd;
  }

  public getAllProduct(): void {
    setTimeout(() => {
      this.spinnerService.showSpinner();
    }, 0);

    this.productService
      .getProductsData$('')
      .subscribe(res => {
        this.products = res?.data;
        if (this.products.length) {
          this.spinnerService.hideSpinner();
          this.getDropDownMenu(res?.data);
        }
      });
  }

  private getDropDownMenu(data: Product[]): void {
    let tempArray: string[] = [];
    data.forEach(item => tempArray.push(item.category));
    this.dropdownMenu = tempArray.filter((elem, item) => tempArray.indexOf(elem) === item);
  }

  private onSubscriptionRemove(): void {
    this.subscriptionRemove = this.popupService.isClosePopup.subscribe((res: ResponsData) => {

      if (!res.isOkBtn) return;

      if (res.isOkBtn && res.data === null) return;

      this.spinnerService.showSpinner();

      setTimeout(() => {
        this.productService.removeProduct(this.products, res.data);

        this.spinnerService.hideSpinner();

        this.popupService.openPopupConfig({
          title: this.popupService.CONFIG_DATA_REMOVE.title,
          bodyText: this.popupService.CONFIG_DATA_REMOVE.bodyText,
          isOkBtn: this.popupService.CONFIG_DATA_REMOVE.isOkBtn,
          isDelBtn: this.popupService.CONFIG_DATA_REMOVE.isDelBtn,
          data: res.data,
        });
      }, 1000);
    });
  }

  public sentDataForForm(data: Product): void {
    let index = this.products.findIndex(product => product.id === data.id);

    if (index != -1) {
      this.productSelect = data;
    }
  }

  public openModalEdit(data: Product) {
    this.sentDataForForm(data);

    this.popupService.openModalConfig({
      title: CONFIG_PRODUCT_EDIT.title,
      labelApi: CONFIG_PRODUCT_EDIT.labelApi,
      labelCategory: CONFIG_PRODUCT_EDIT.labelCategory,
      labelDescription: CONFIG_PRODUCT_EDIT.labelDescription,
      isOkBtn: CONFIG_PRODUCT_EDIT.isOkBtn,
      isDelBtn: CONFIG_PRODUCT_EDIT.isDelBtn,
      isRadioBtn: CONFIG_PRODUCT_EDIT.isRadioBtn,
      data: data,
    });

    this.isOpenPopupEdit = !this.isOpenPopupEdit;
  }

  public displayAll(): void {
    this.getAllProduct();
    this.productSort = [];
  }
}
