import { Component } from '@angular/core';
import { HOVER_TIPS } from '../../shared/constants/hover-tips';

@Component({
  selector: 'tips',
  templateUrl: './tips.html',
  styleUrls: ['./tips.scss']
})
export class Tips {
  public tipsButtonName: string = 'TIPS';

  public readonly HOVER_TIPS: string[] = HOVER_TIPS;
}
