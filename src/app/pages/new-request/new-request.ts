import { Component } from '@angular/core';

@Component({
  selector: 'new-request',
  templateUrl: './new-request.html',
  styleUrls: ['./new-request.scss']
})
export class NewRequest {}
