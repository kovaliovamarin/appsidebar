import { Component } from '@angular/core';

import { ProductService } from '../../services/product.service/product.service';

@Component({
  selector: 'form-page',
  templateUrl: './form-page.html',
  styleUrls: ['./form-page.scss']
})
export class FormPage {
  constructor(
    public productService: ProductService,
  ) { }
}
